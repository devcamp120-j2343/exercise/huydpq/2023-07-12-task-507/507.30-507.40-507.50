const express = require ("express")
const path = require("path");

const app = express()

const port = 8000;

app.use(express.static(__dirname + "/views/Course365"))
app.use(express.static(__dirname + "/views/Fresh.project"))
app.use(express.static(__dirname + "/views/Pizza 365-v1.9"))
app.get("/course", (req, res ) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/Course365/index.html"));

})
app.get("/fresh", (req, res ) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/Fresh.project/index.html"));
    

})
app.get("/pizza", (req, res ) => {
    console.log(__dirname)
    res.sendFile(path.join(__dirname + "/views/Pizza 365-v1.9/Pizza 365 v1.9.html"));
    

})
app.listen(port, () => {
    console.log(`dang chay cong ${port}`);
})